<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->bigIncrements( 'id' );
            $table->string( 'email', 60 );
            $table->string( 'name', 60 );
            $table->string( 'last_name', 50 );
            $table->string( 'maternal_surnmae', 50 );
            $table->unsignedBigInteger( 'user_id' );
            $table->timestamps();
            $table->softDeletes();

            $table->foreign( 'user_id' )
                    ->references( 'id' )
                    ->on( 'users' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
