<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_information', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string( 'name', 60 )->nullable();
            $table->string( 'last_name', 50 )->nullable();
            $table->string( 'maternal_surname', 50 )->nullable();
            $table->string( 'rfc', 13 )->nullable();
            $table->text( 'address' )->nullable();
            $table->string( 'url_image' )->nullable();
            $table->string( 'phone', 21 )->nullable();
            $table->unsignedBigInteger( 'user_id' );
            $table->timestamps();
            $table->softDeletes();

            $table->foreign( 'user_id' )
                    ->references( 'id' )
                    ->on( 'users' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_information');
    }
}
