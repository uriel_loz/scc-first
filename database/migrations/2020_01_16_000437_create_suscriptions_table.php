<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suscriptions', function (Blueprint $table) {
            $table->bigIncrements( 'id' );
            $table->date( 'start_date' );
            $table->date( 'end_date' );
            $table->char( 'type', 1 );
            $table->char( 'pay', 1 );
            $table->integer( 'number_contribuidor', 5 );
            $table->integer( 'number_email', 5 );
            $table->unsignedBigInteger( 'package_id' );
            $table->unsignedBigInteger( 'user_id' );
            $table->timestamps();
            $table->softDeletes();

            $table->foreign( 'package_id' )
                        ->references( 'id' )
                        ->on( 'packages' );

            $table->foreign( 'user_id' )
                        ->references( 'id' )
                        ->on( 'users' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suscriptions');
    }
}
