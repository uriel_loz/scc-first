<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Email extends Model
{
    use SoftDeletes;

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	*/
    protected $fillable = [
        'email',
        'name',
        'last_name',
        'maternal_surname',
        'user_id',
    ];

    public function email()
    {
        return $this->belongsTo( User::class );
    }
}
