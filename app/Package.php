<?php

namespace App;

use App\Suscription;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	*/
    protected $fillable = [
        'title',
        'duration_suscription',
        'user_quantity'
    ];

    public function suscriptions()
    {
        return $this->hasMany( Suscription::class );
    }
}
