<?php

namespace App;

use App\User;
use App\Package;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suscription extends Model
{
    use SoftDeletes;

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	*/
    protected $fillable = [
        'start_date',
        'end_date',
        'type',
        'pay',
        'number_contribuidor',
        'number_emails',
        'package_id',
        'user_id',
    ];

    public function package()
    {
        return $this->belongsTo( Package::class );
    }

    public function user()
    {
        return $this->belongsTo( User::class );
    }
}
