<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
    use SoftDeletes;

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	*/
    protected $fillable = [
        'title',
        'url_file',
    ];

    public function users()
    {
        return $this->belongsToMany( User::class, 'module_user', 'user_id', 'module_id' );
    }
}
