<?php

namespace App;

use App\Email;
use App\Module;
use App\SessionLog;
use App\Suscription;
use App\UserInformation;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	*/
    protected $fillable = [
        'email',
        'email_verified_at',
        'role',
		'password',
		'parent'
    ];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	*/
	protected $dates = [
		'deleted_at',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	*/
	protected $hidden = [
		'password',
		'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	*/
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	public function emails()
	{
		return $this->hasMany( Email::class );
    }
    
    public function modules()
    {
        return $this->belongsToMany( Module::class, 'module_user', 'user_id', 'module_id');
    }

    public function sessionLog()
    {
        return $this->hasMany( SessionLog::class );
    }

    public function suscriptions()
    {
        return $this->hasMany( Suscription::class );
    }

    public function userInformation()
    {
        return $this->hasOne( UserInformation::class );
    }
}
