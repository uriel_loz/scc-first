<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserInformation extends Model
{
    use SoftDeletes;

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	*/
    protected $fillable = [
        'name',
		'last_name',
		'maternal_surname',
		'rfc',
		'address',
		'url_image',
		'phone',
		'user_id'
    ];

	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	*/
	protected $dates = [
		'deleted_at',
	];

	public function user()
	{
		return $this->belongsTo( User::class );
	}
}
